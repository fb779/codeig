<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/*
* Changes:
* 1. This project contains .htaccess file for windows machine.
*    Please update as per your requirements.
*    Samples (Win/Linux): http://stackoverflow.com/questions/28525870/removing-index-php-from-url-in-codeigniter-on-mandriva
*
* 2. Change 'encryption_key' in application\config\config.php
*    Link for encryption_key: http://jeffreybarke.net/tools/codeigniter-encryption-key-generator/
* 
* 3. Change 'jwt_key' in application\config\jwt.php
*
*/

class Auth extends REST_Controller
{
	/**
	* URL: /auth/token
	* Method: GET
	*/
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('getdata');
	}
	
  // auth/token?email=Y29ycmVvQG1haWwuY29t&password=MTIzNDU2
  // auth/token/email/Y29ycmVvQG1haWwuY29t/password/MTIzNDU2
	
	public function token_get()
	{
		$output["contract"] = array(
			"type" => "post",
			"params" => array(
				"email" => "BASE64_ENCODE",
				"password" => "BASE64_ENCODE"
				),
			"respose_ok" => array(
				"status" => TRUE,
				"token" => "ENCRYPT_TOKEN",
				"http" => "200"
				),
			"respose_error" => array(
				"status" => FALSE,
				"respose" => "Unauthorised",
				"http" => "401"
				),
			"url" => base_url("/auth/token")
			);

		$this->set_response($output, REST_Controller::HTTP_OK);
	}

	/**
	* URL: /auth/token
	* Method: POST
	* Header Key: Authorization
	* Value: Auth token generated in GET call
	*/
	public function token_post()
	{
		$params = $this->post();
		$data = NULL;
		if(isset($params['email']) && isset($params['password']))
		{
			$loginParams['email'] = base64_decode($params['email']);
			$loginParams['password'] = base64_decode($params['password']);
			$data = $this->login($loginParams);
		}

		if(!is_null($data)){
			$tokenData = array();
			$tokenData['id'] = $data; 
			$output['token'] = AUTHORIZATION::generateToken($tokenData);
			
			//header("Access-Control-Allow-Origin: *");
			/*header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
	  		header('Access-Control-Allow-Credentials: true');  
	  		header('Access-Control-Max-Age: 86400');   */
	  		$this->set_response($output, REST_Controller::HTTP_OK);
	  	}else{
	  		$unauthorised = array(
	  			"status" => FALSE,
	  			"respose" => "Unauthorised"
	  			);
	  		$this->set_response($unauthorised, REST_Controller::HTTP_UNAUTHORIZED);
	  	}      
	  }

	  public function authorization_get()
	  {

	  }

	  public function authorization_post()
	  {
	  	$headers = $this->input->request_headers();

	  	if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
	  		$decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
	  		if ($decodedToken != false) {
	  			$this->set_response($decodedToken, REST_Controller::HTTP_OK);
	  			return;
	  		}
	  	}

	  	$unauthorised = array(
	  		"status" => FALSE,
	  		"respose" => "Unauthorised"
	  		);
	  	$this->set_response($unauthorised, REST_Controller::HTTP_UNAUTHORIZED);
	  }

	  private function login($params)
	  {
	  	$result = NULL;
	  	$resultLogin = $this->getdata->login($params); 
	  	if(!is_null($resultLogin) && $resultLogin['password'] == $params["password"])
	  		$result = md5($params["password"]);
	  	return $result;
	  }
	}