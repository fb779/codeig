<?php

class Getdata extends CI_Model {

	function __construct()
	{        
		parent::__construct();
	}

	public function login($params)
	{
		$this->db->select('*');
    $query = $this->db->get_where('users', $params, 1);
		$result = $query->row_array();
		return $result;
	}

}