<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejemplo Angular</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body ng-app="myApp" ng-controller="myController">
	<div class="container-fluid">
		<div class="container">
			<div id="base-url" class="hide">
				<?php echo base_url(); ?>
			</div>
			<h1>
				Formulario con services
			</h1>
			<form name="nombreFormulario" id="nombreFormulario">
				<div class="col-md-8">
					<div class="form-group">
						<label for="mail">Correo</label>
						<input type="email" class="form-control" id="mail" name="mail" 
						ng-model="Formulario.correo"
						ng-pattern="/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/" placeholder="Correo" required>
					</div>
				</div>
				<div class="col-md-4">
					<div ng-messages="nombreFormulario.mail.$error" ng-if="nombreFormulario.mail.$touched" class="text-right">
						<div ng-message="required">
							<span class="alert alert-danger"><strong>Correo</strong> Es requerido</span>
						</div>
						<div ng-message="pattern">
							<span class="alert alert-warning"><strong>Correo</strong> Deber se un correo valido</span>	
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" class="form-control" id="name" name="name" ng-model="Formulario.nombre" placeholder="Nombre" required>
					</div>
				</div>
				<div class="col-md-4">
					<div ng-messages="nombreFormulario.name.$error" ng-if="nombreFormulario.name.$touched" class="text-right">
						<div ng-message="required">
							<span class="alert alert-danger"><strong>Nombre</strong> Es requerido</span>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<label for="dni">Documento</label>
						<input type="text" class="form-control" id="dni" name="dni" 
						ng-model="Formulario.documento" 
						ng-pattern="/^([0-9])*$/" placeholder="Documento" required>
					</div>
				</div>
				<div class="col-md-4">
					<div ng-messages="nombreFormulario.dni.$error" ng-if="nombreFormulario.dni.$touched" class="text-right">
						<div ng-message="required">
							<span class="alert alert-danger"><strong>Documento</strong> Es requerido</span>
						</div>
						<div ng-message="pattern">
							<span class="alert alert-warning"><strong>Documento</strong> Deber se un numero valido</span>	
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<label for="dpt">Departamento</label>
						<select class="form-control" id="dpt" name="dpt" 
						ng-model="Formulario.departamento" 
						ng-change="getmunicipios({'id':Formulario.departamento})" required>
						<option ng-repeat="departamento in departamentos" value="{{departamento.id}}">{{departamento.nombre}}</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div ng-messages="nombreFormulario.dpt.$error" ng-if=" !theForm.valid" class="text-right">
					<div ng-message="required">
						<span class="alert alert-danger"><strong>Departamento</strong> Es requerido</span>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label for="exampleInputFile">Municipio</label>
					<select class="form-control" id="muni" name="muni" ng-model="Formulario.municipio" required>
						<option ng-repeat="municipio in municipios" value="{{municipio.id}}">{{municipio.nombre}}</option>
					</select>
				</div>
			</div>
				<div class="col-md-4">
				<div ng-messages="nombreFormulario.muni.$error" ng-if=" !theForm.valid" class="text-right">
					<div ng-message="required">
						<span class="alert alert-danger"><strong>Municipio</strong> Es requerido</span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<span ng-show="showSave" class="alert alert-success"><strong>Se Guardaron los datos correctamento</strong> </span>
			</div>
			<div class="col-md-12">
				<span ng-show="errorSave" class="alert alert-danger"><strong>Error al guardar los datos</strong> </span>
			</div>
			<div class="col-md-12">
				<span ng-show="errorValid" class="alert alert-warning"><strong>Complete los datos requeridos</strong> </span>
			</div>
			<div class="col-md-8 text-right">
			<br><br><br>
				<button type="submit" class="btn btn-primary" ng-click="saveForm({valid: nombreFormulario.$valid})">Enviar</button>
			</div>
		</form>
	</div>
</div>
</body>
<!-- Jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- Boopttrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-messages.js"></script>

<!-- My JS Remplazar con la ruta propia usando el metodo de en php base_url() -->
<script src="<?php echo base_url("/assets/js/basicos/angular-services2.js"); ?>" rel="stylesheet"></script>
</html>