<!DOCTYPE html>
<html >
<head>
	<meta charset="UTF-8">
	<title>Angular 4 Proyecto</title>
	<link href="<?php echo base_url("assets/vendor/node_modules/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet"/>
	<link href="<?php echo base_url("/assets/vendor/prism/prism.css"); ?>" rel="stylesheet">
</head>
<body>
	<h1>Angular 4 Proyecto</h1>
	
	<div class="col-md-12">
		<legend>
			<h5>
				Crear un nuevo proyecto Angular con angular-cli - <small>Consola</small>
			</h5>
		</legend>
		<pre class="language-bash line-numbers" data-line="" style="height:160px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1-config"); ?>"></pre>
		<legend>
			<h5>
				Instalar Bootstrap  - <small>Consola</small>
			</h5>
		</legend>
		<pre class="language-bash line-numbers" data-line="" style="height:50px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1-config1"); ?>"></pre>
		<legend>
			<h5>
				Importar el modulo en el main  - <small>src/main.ts</small>
			</h5>
		</legend>
		<pre class="language-typescript line-numbers" data-line="3" style="height:260px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1-main.ts"); ?>"></pre>
		<legend>
			<h5>
				Importar el modulo en el root  - <small>src/app/app.module.ts</small>
			</h5>
		</legend>
		<pre class="language-typescript line-numbers" data-line="3,13" style="height:370px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1-app.module.ts"); ?>"></pre>
		<legend>
			<h5>
				Importar los estilos  - <small>src/app/app.component.css</small>
			</h5>
		</legend>
		<pre class="language-css line-numbers" data-line="1" style="height:50px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1-app.component.css"); ?>"></pre>
		<legend>
			<h5>
				Cambiar ruta compilación  - <small>.angular-cli.json</small>
			</h5>
		</legend>
		<pre class="language-json line-numbers" data-line="9" style="height:400px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1.angularcli.json"); ?>"></pre>
		<legend>
			<h5>
				Incluir funcion para la ruta en codeigniter  - <small>application/controller/Curso.php</small>
			</h5>
		</legend>
		<pre class="language-php line-numbers" data-line="" style="height:170px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1.cursoctrl.html"); ?>"></pre>
		
		<legend>
			<h5>
				Crear una vista para las pruebas - <small>application/views/angular.php</small>
			</h5>
		</legend>
		<pre class="language-markup line-numbers" data-line="" style="height:290px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1.angular.html"); ?>"></pre>
		<legend>
			<h5>
				Dentro de la etiqueta "body" remplazar "Pegar aquí"  - <small>views/angular.php</small>
			</h5>
		</legend>
		<pre class="language-markup " data-line="" style="height:200px;" >
		<code>
&lt;!-- Pegar aquí --&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;&lt;?php echo base_url('assets/testangular/inline.bundle.js') ?&gt;&quot;&gt;&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;&lt;?php echo base_url('assets/testangular/polyfills.bundle.js') ?&gt;&quot;&gt;&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;&lt;?php echo base_url('assets/testangular/styles.bundle.js') ?&gt;&quot;&gt;&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;&lt;?php echo base_url('assets/testangular/vendor.bundle.js') ?&gt;&quot;&gt;&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;&lt;?php echo base_url('assets/testangular/main.bundle.js') ?&gt;&quot;&gt;&lt;/script&gt;
		</code>
		</pre>
		<legend>
			<h5>
				Desde consola en la carpeta del aplicativo Angular construir la aplicación  - <small>Consola</small>
			</h5>
		</legend>
		<pre class="bash line-numbers" data-line="" style="height:50px;" data-src="<?php echo base_url("/curso/resultado/angular4-app1-config2"); ?>"></pre>
		<legend>
			<h5>
				Una vez generado en la carpeta angulartest cargar la url https://misitio/curso/angular - <small>views/angulartest/index.html</small>
			</h5>
		</legend>
	</div>

	
	
</body>
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- Prims -->
<script src="<?php echo base_url("/assets/vendor/prism/prism.js"); ?>" rel="stylesheet" ></script>

<!-- Bootstrap -->
<script src="<?php echo base_url("/assets/vendor/node_modules/bootstrap/dist/js/bootstrap.min.js"); ?>" rel="stylesheet"></script>
</html>