<nav class="navbar navbar-default navbar-dane">
	<div class="container-fluid">
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<a class="navbar-brand" href="#">
				<img alt="Logo" class="img-responsive" src="#">
			</a>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div class="title-form-lg text-center">
				<div class="titulo-formulario">
					<span class="normal-weight">Curso AngularJS</span>
					<span class="bold-weight">Inicio</span>
				</div>
			</div>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		</div>
		<div class="pattern-line pattern-line-image">
		</div>
	</div>
</nav>