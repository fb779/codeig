<div class="container">
	<a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Inicio</a>
	<br>
	<br>
	<div id="base-url" class="hide">
		<?php echo base_url(); ?>
	</div>
</div>
<div class="container-fluid" id="viewApp" ng-app="viewCodeApp" ng-controller="angular4App1">
	<div class="col-md-12">
		<div class="thumbnail">
			https://codecraft.tv/courses/angular/quickstart/nesting-components-and-inputs/
			<div id="lecture-content">
        <div id="toc" class="toc">

<ul class="sectlevel1">
<li><a href="#_learning_outcomes">Learning Outcomes</a></li>
<li><a href="#_create_configure_multiple_components">Create &amp; Configure Multiple Components</a>
<ul class="sectlevel2">
<li><a href="#_jokecomponent">JokeComponent</a></li>
<li><a href="#_jokelistcomponent">JokeListComponent</a></li>
<li><a href="#_appcomponent">AppComponent</a></li>
<li><a href="#_configuring_multiple_components">Configuring multiple Components</a></li>
</ul>
</li>
<li><a href="#_input_property_binding_on_a_custom_component">Input Property Binding on&nbsp;a Custom Component</a></li>
<li><a href="#_summary">Summary</a></li>
<li><a href="#_listing">Listing</a></li>
</ul>
</div>
<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>An&nbsp;application in&nbsp;Angular is a&nbsp;set of&nbsp;custom components glued together in&nbsp;<abbr>HTML</abbr> via inputs and&nbsp;outputs.</p>
</div>
<div class="paragraph">
<p>So far we’ve only built applications with a&nbsp;single component, our goal now is to&nbsp;start building applications that are <em>composed</em> of&nbsp;multiple components working together.</p>
</div>
<div class="paragraph">
<p>Breaking up an&nbsp;application into multiple logical components makes it easier to:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>Architect an&nbsp;application as&nbsp;it grows in&nbsp;complexity.</p>
</li>
<li>
<p><nobr>Re-use</nobr> common components in&nbsp;multiple places.</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>The&nbsp;goal of&nbsp;this lecture is to&nbsp;break up our small application into 3 components and&nbsp;start <em>gluing</em> them together.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_learning_outcomes"><a href="#_learning_outcomes">Learning Outcomes</a></h2>
<div class="sectionbody">
<div class="ulist">
<ul>
<li>
<p>How to&nbsp;create and&nbsp;configure multiple components in&nbsp;one application.</p>
</li>
<li>
<p>How to&nbsp;enable Input Property Binding on&nbsp;a custom component so components can communicate with each other.</p>
</li>
</ul>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_create_configure_multiple_components"><a href="#_create_configure_multiple_components">Create &amp; Configure Multiple Components</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>If you think of&nbsp;a typical webpage we can normally break it down into a&nbsp;set of&nbsp;logical components each with its own view, for&nbsp;example most webpages can be broken up into a&nbsp;header, footer and&nbsp;perhaps a&nbsp;sidebar.</p>
</div>
<div class="paragraph">
<p>We are going to&nbsp;break up our application into a&nbsp;root <code>AppComponent</code>, this component won’t have any functionality and&nbsp;will just contain other components.</p>
</div>
<div class="paragraph">
<p>This component will hold our <code>JokeListComponent</code> and&nbsp;our <code>JokeListComponent</code> will hold a&nbsp;list of&nbsp;<code>JokeComponents</code>.</p>
</div>
<div class="callout callout-note"><h4>Note</h4><div>
Most Angular apps will have a&nbsp;root component called <em>AppRoot</em> or&nbsp;<em>AppComponent</em>, this typically just acts as&nbsp;a container to&nbsp;hold other components.
</div></div>
<div class="paragraph">
<p>Our components will therefore nest something like the&nbsp;below image:</p>
</div>
<div class="imageblock">
<div class="content">
<img src="/assets/images/courses/angular/1.quickstart/nesting.png" alt="Component Nesting">
</div>
</div>
<div class="callout callout-note"><h4>Note</h4><div>
For&nbsp;the <em>convenience</em> of&nbsp;learning we are going to&nbsp;keep everything in&nbsp;one file. When building Angular apps the&nbsp;recommended approach is to&nbsp;have one component per file.
</div></div>
<div class="sect2">
<h3 id="_jokecomponent"><a href="#_jokecomponent">JokeComponent</a></h3>
<div class="paragraph">
<p>This looks similar to&nbsp;our previous <code>JokeListComponent</code> we just removed the&nbsp;<code>NgFor</code> since this component will now render a&nbsp;single joke.</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">@Component({
  selector: 'joke',
  template: `
&lt;div class="card card-block"&gt;
  &lt;h4 class="card-title"&gt;{{joke.setup}}&lt;/h4&gt;
  &lt;p class="card-text"
     [hidden]="joke.hide"&gt;{{joke.punchline}}&lt;/p&gt;
  &lt;a (click)="joke.toggle()"
     class="btn btn-warning"&gt;Tell Me
  &lt;/a&gt;
&lt;/div&gt;
  `
})
class JokeComponent {
  joke: Joke;
}</code></pre>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_jokelistcomponent"><a href="#_jokelistcomponent">JokeListComponent</a></h3>
<div class="paragraph">
<p>We’ve broken out a&nbsp;joke into its own <code>JokeComponent</code> so now we change the&nbsp;<code>JokeListComponent</code> template to&nbsp;contain multiple <code>JokeComponent</code> components instead.</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">@Component({
  selector: 'joke-list',
  template: `
&lt;joke *ngFor="let j of jokes"&gt;&lt;/joke&gt;
  `
})
class JokeListComponent {
  jokes: Joke[];

  constructor() {
    this.jokes = [
      new Joke("What did the cheese say when it looked in the mirror?", "Hello-me (Halloumi)"),
      new Joke("What kind of cheese do you use to disguise a small horse?", "Mask-a-pony (Mascarpone)"),
      new Joke("A kid threw a lump of cheddar at me", "I thought ‘That’s not very mature’"),
    ];
  }
}</code></pre>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_appcomponent"><a href="#_appcomponent">AppComponent</a></h3>
<div class="paragraph">
<p>Our final component is our new top level <code>AppComponent</code>, this just holds an&nbsp;instance of&nbsp;the <code>JokeListComponent</code>.</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">@Component({
  selector: 'app',
  template: `
&lt;joke-list&gt;&lt;/joke-list&gt;
  `
})
class AppComponent {
}</code></pre>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_configuring_multiple_components"><a href="#_configuring_multiple_components">Configuring multiple Components</a></h3>
<div class="paragraph">
<p>In&nbsp;order to&nbsp;use our new components we need to&nbsp;add them to&nbsp;the declarations on&nbsp;our <code>NgModule</code>.</p>
</div>
<div class="paragraph">
<p>And&nbsp;since we’ve changed our top level component we need to&nbsp;set that in&nbsp;the bootstrap property as&nbsp;well as&nbsp;change our <code>index.html</code> to&nbsp;use the&nbsp;<code>&lt;app&gt;&lt;/app&gt;</code> root component instead.</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">@NgModule({
  imports: [BrowserModule],
  declarations: [
    AppComponent,
    JokeComponent,
    JokeListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}</code></pre>
</div>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="html">&lt;body class="container m-t-1"&gt;
  &lt;app&gt;&lt;/app&gt;
&lt;/body&gt;</code></pre>
</div>
</div>
<div class="paragraph">
<p>In&nbsp;Angular we need to&nbsp;be explicit regarding what components &amp; directives are going to&nbsp;use in&nbsp;our Angular Module by&nbsp;either adding them to&nbsp;the <code>imports</code> or&nbsp;<code>declarations</code> property.</p>
</div>
<div class="paragraph">
<p>In&nbsp;Angular 1 each directive when added via a&nbsp;script tag was globally available, which made it convenient for&nbsp;smaller projects but a&nbsp;problem for&nbsp;larger ones. Issues like name clashes came up often as&nbsp;different third party libraries often used the&nbsp;same names.</p>
</div>
<div class="paragraph">
<p>With Angular two third party libraries can export the&nbsp;same name for&nbsp;components but only the&nbsp;version we explicitly include into our Angular Module will be used.</p>
</div>
<div class="callout callout-tip"><h4>Tip</h4><div>
The&nbsp;<nobr>built-in</nobr> directives we are using such as&nbsp;<code>NgFor</code> are all defined in&nbsp;<code>CommonModule</code> which is again included in&nbsp;<code>BrowserModule</code> which we have already added to&nbsp;our <code>NgModule</code> imports.
</div></div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_input_property_binding_on_a_custom_component"><a href="#_input_property_binding_on_a_custom_component">Input Property Binding on&nbsp;a Custom Component</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>If we ran the&nbsp;application now we would see just some empty boxes with some errors in&nbsp;the console, like so:</p>
</div>
<div class="imageblock">
<div class="content">
<img src="/assets/images/courses/angular/1.quickstart/input-error.png" alt="Binding Error">
</div>
</div>
<div class="paragraph">
<p>The&nbsp;errors should read something like:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code>class JokeComponent - inline template:2:25 caused by: Cannot read property 'setup' of undefined</code></pre>
</div>
</div>
<div class="paragraph">
<p>The&nbsp;above should give you a&nbsp;hint about what’s going on</p>
</div>
<div class="olist arabic">
<ol class="arabic">
<li>
<p>It’s something to&nbsp;do with the&nbsp;<code>JokeComponent</code></p>
</li>
<li>
<p>It’s something to&nbsp;do with the&nbsp;template.</p>
</li>
<li>
<p>It’s something to&nbsp;do with the&nbsp;<code>setup</code> property.</p>
</li>
</ol>
</div>
<div class="paragraph">
<p>So if we look at&nbsp;the offending part of&nbsp;our <code>JokeComponent</code> template:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="html">&lt;h4 class="card-title"&gt;{{joke.setup}}&lt;/h4&gt;</code></pre>
</div>
</div>
<div class="paragraph">
<p>Essentially <code>Cannot read property 'setup' of undefined</code> in&nbsp;the context of&nbsp;<code>joke.setup</code> means that <code>joke</code> is undefined, it’s blank</p>
</div>
<div class="paragraph">
<p>If you remember from our <code>JokeComponent</code> class we do have a&nbsp;property called <code>joke</code>:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">class JokeComponent {
  joke: Joke;
}</code></pre>
</div>
</div>
<div class="paragraph">
<p>And&nbsp;we are looping and&nbsp;creating <code>JokeComponents</code> in&nbsp;our <code>JokeListComponent</code>, like so:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="html">&lt;joke *ngFor="let j of jokes"&gt;&lt;/joke&gt;</code></pre>
</div>
</div>
<div class="paragraph">
<p>But we are not setting the&nbsp;property <code>joke</code> of&nbsp;our <code>JokeComponent</code> to&nbsp;anything, which is why it’s <code>undefined</code>.</p>
</div>
<div class="paragraph">
<p>Ideally we want to&nbsp;write something like this:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="html">&lt;joke *ngFor="let j of jokes" [joke]="j"&gt;&lt;/joke&gt;</code></pre>
</div>
</div>
<div class="paragraph">
<p>In&nbsp;just the&nbsp;same way as&nbsp;we bound to&nbsp;the <code>hidden</code> property of&nbsp;the <code>p</code> tag in&nbsp;the element above we want to&nbsp;bind to&nbsp;the <code>joke</code> property of&nbsp;our <code>JokeComponent</code>.</p>
</div>
<div class="paragraph">
<p>Even though our <code>JokeComponent</code> has a&nbsp;<code>joke</code> property we can’t bind to&nbsp;it using the&nbsp;<code>[]</code> syntax, we need to&nbsp;explicitly mark it as&nbsp;an <em>Input</em> property on&nbsp;our <code>JokeComponent</code>.</p>
</div>
<div class="paragraph">
<p>We do this by&nbsp;pre-pending the&nbsp;<code>joke</code> property in&nbsp;the component with a&nbsp;new annotation called <code>@Input</code>, like so:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">import { Input } from '@angular/core';
.
.
.
class JokeComponent {
  @Input() joke: Joke;
}</code></pre>
</div>
</div>
<div class="paragraph">
<p>This tells Angular that the&nbsp;<code>joke</code> property is an&nbsp;<em>input</em> property and&nbsp;therefore in&nbsp;<abbr>HTML</abbr> we can bind to&nbsp;it using the&nbsp;<code>[]</code> input property binding syntax.</p>
</div>
<div class="paragraph">
<p>This <code>@Input</code> now becomes part of&nbsp;the <em>public interface</em> of&nbsp;our component.</p>
</div>
<div class="paragraph">
<p>Lets say at&nbsp;some future point we decided to&nbsp;change the&nbsp;<code>joke</code> property of&nbsp;our JokeComponent to&nbsp;perhaps just <code>data</code>, like so:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">class JokeComponent {
  @Input() data: Joke;
}</code></pre>
</div>
</div>
<div class="paragraph">
<p>Because this input is part of&nbsp;the public interface for&nbsp;our component we would also need to&nbsp;change all the&nbsp;input property bindings every where our component is used, like so:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="html">&lt;joke *ngFor="let j of jokes" [data]="j"&gt;&lt;/joke&gt;</code></pre>
</div>
</div>
<div class="paragraph">
<p>Not a&nbsp;great thing to&nbsp;ask the&nbsp;consumers of&nbsp;your component to&nbsp;have to&nbsp;do.</p>
</div>
<div class="paragraph">
<p>This is a&nbsp;common problem so to&nbsp;avoid expensive refactors the&nbsp;<code>@Input</code> annotation takes a&nbsp;parameter which is the&nbsp;name of&nbsp;the input property to&nbsp;the outside world, so if we changed our component like so:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">class JokeComponent {
  @Input('joke') data: Joke;
}</code></pre>
</div>
</div>
<div class="paragraph">
<p>To&nbsp;the outside world the&nbsp;input property name is still joke and&nbsp;we could keep the&nbsp;<code>JokeListComponent</code> template the&nbsp;same as&nbsp;before:</p>
</div>
<div class="listingblock">
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="html">&lt;joke *ngFor="let j of jokes" [joke]="j"&gt;&lt;/joke&gt;</code></pre>
</div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_summary"><a href="#_summary">Summary</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p>An&nbsp;Angular application should be broken down into small logical components which are glued together in&nbsp;<abbr>HTML</abbr>.</p>
</div>
<div class="paragraph">
<p>Its normal to&nbsp;have one root component called <code>AppComponent</code> which acts as&nbsp;the root node in&nbsp;the component tree.</p>
</div>
<div class="paragraph">
<p>We need to&nbsp;explicitly declare all components in&nbsp;the applications root <code>NgModule</code>.</p>
</div>
<div class="paragraph">
<p>We can make properties of&nbsp;our custom components input bindable with the&nbsp;<code>[]</code> syntax by&nbsp;pre-pending them with the&nbsp;<code>@Input</code> annotation.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_listing"><a href="#_listing">Listing</a></h2>
<div class="sectionbody">
<div class="paragraph">
<p><a href="http://plnkr.co/edit/LKj9OAoUMIUZhDS5HAiP?p=preview" class="bare">http://plnkr.co/edit/LKj9OAo<abbr>UMIUZ</abbr>hDS5HAiP?p=preview</a></p>
</div>
<div class="listingblock">
<div class="title">Listing 1. script.ts</div>
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule, Input}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Component} from '@angular/core';

class Joke {
  public setup: string;
  public punchline: string;
  public hide: boolean;

  constructor(setup: string, punchline: string) {
    this.setup = setup;
    this.punchline = punchline;
    this.hide = true;
  }

  toggle() {
    this.hide = !this.hide;
  }
}

@Component({
  selector: 'joke',
  template: `
&lt;div class="card card-block"&gt;
  &lt;h4 class="card-title"&gt;{{data.setup}}&lt;/h4&gt;
  &lt;p class="card-text"
     [hidden]="data.hide"&gt;{{data.punchline}}&lt;/p&gt;
  &lt;a (click)="data.toggle()"
     class="btn btn-warning"&gt;Tell Me
  &lt;/a&gt;
&lt;/div&gt;
  `
})
class JokeComponent {
  @Input('joke') data: Joke;
}

@Component({
  selector: 'joke-list',
  template: `
&lt;joke *ngFor="let j of jokes" [joke]="j"&gt;&lt;/joke&gt;
  `
})
class JokeListComponent {
  jokes: Joke[];

  constructor() {
    this.jokes = [
      new Joke("What did the cheese say when it looked in the mirror?", "Hello-me (Halloumi)"),
      new Joke("What kind of cheese do you use to disguise a small horse?", "Mask-a-pony (Mascarpone)"),
      new Joke("A kid threw a lump of cheddar at me", "I thought ‘That’s not very mature’"),
    ];
  }
}

@Component({
  selector: 'app',
  template: `
&lt;joke-list&gt;&lt;/joke-list&gt;
  `
})
class AppComponent {
}

@NgModule({
  imports: [BrowserModule],
  declarations: [
    AppComponent,
    JokeComponent,
    JokeListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);</code></pre>
</div>
</div>
<div class="listingblock">
<div class="title">Listing 2. index.html</div>
<div class="content">
<pre class="pygments highlight"><span class="language-name"></span><a class="btn btn-sm btn-purple clipboard-copy" data-original-title="Copied!">Copy</a><code data-lang="typescript">&lt;!DOCTYPE html&gt;
&lt;!--suppress ALL --&gt;
&lt;html&gt;
&lt;head&gt;
  &lt;link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css"&gt;

  &lt;script src="https://unpkg.com/core-js/client/shim.min.js"&gt;&lt;/script&gt;
  &lt;script src="https://unpkg.com/zone.js@0.7.4?main=browser"&gt;&lt;/script&gt;
  &lt;script src="https://unpkg.com/reflect-metadata@0.1.8"&gt;&lt;/script&gt;
  &lt;script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"&gt;&lt;/script&gt;
	&lt;script src="systemjs.config.js"&gt;&lt;/script&gt;
	&lt;script&gt;
		System.import('script.ts').catch(function (err) {
			console.error(err);
		});
	&lt;/script&gt;
&lt;/head&gt;

&lt;body class="container m-t-1"&gt;
	&lt;app&gt;&lt;/app&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>
</div>
</div>
</div>
</div>

      </div>
			
		</div>
	</div>
</div>