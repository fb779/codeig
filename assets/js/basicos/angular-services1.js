var $baseUrl = $('div#base-url').html();

var app = angular.module('myApp', []);

app.controller('myController', ['$scope', function($scope){
  console.log({"Controlador": "myController services1"});

  $scope.departamentos = [
  {"id":1, "nombre":"ANTIOQUIA"},
  {"id":2, "nombre":"ATLANTICO"},
  {"id":3, "nombre":"BOLIVAR"}
  ];

  $scope.todosMunicipios = { 
    "1": [
    {"id":1, "nombre":"MEDELLIN"},
    {"id":2, "nombre":"ABEJORRAL"},
    {"id":3, "nombre":"ABRIAQUI"}
    ],
    "2": [
    {"id":4, "nombre":"GALAPA"},
    {"id":5, "nombre":"JUAN DE ACOSTA"},
    {"id":6, "nombre":"LURUACO"}
    ],
    "3": [
    {"id":7, "nombre":"ACHI"},
    {"id":8, "nombre":"ALTOS DEL ROSARIO"},
    {"id":9, "nombre":"ARENAL"}
    ]
  };

  $scope.municipios = {};
  $scope.form = {};

  $scope.getmunicipios = function(params){
    if(typeof params.id != 'undefied')
        $scope.municipios = $scope.todosMunicipios[params.id];
  };

}]);