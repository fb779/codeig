import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Component} from '@angular/core';

@Component({
  selector: 'myapp',
  templateUrl: "http://port-8080.angular-brisaning208623.codeanyapp.com/curso/resultado/ts-app3-template"
})
class MyappComponent {
  objecto: Object[];
  
  constructor() {
    this.objecto = [
      {
        titulo: "Vencer el mal",
        mensaje: "El mal se vence alciendo el bien",
        hide: true
      },
      {
        titulo: "Secreto del amor",
        mensaje: "Sólo se puede amar a otros, cuando se tiene amor propio",
        hide: true
      },
      {
        titulo: "El perdón",
        mensaje: "Mira los niños, ellos olvidan las ofensas porque su mente solo retiene lo importante, que les permite crecer y aprender",
        hide: true
      }
    ];
  }
  
  toggle(obj){
    obj.hide = !obj.hide;
  }
}

@NgModule({
  imports: [BrowserModule],
  declarations: [MyappComponent],
  bootstrap: [MyappComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);