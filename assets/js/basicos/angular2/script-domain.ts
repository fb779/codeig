import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Component} from '@angular/core';

class Myapp {
  public titulo: string;
  public mensaje: string;
  public hide: boolean;
  
  constructor(params: Object){
    this.titulo = params.titulo;
    this.mensaje = params.mensaje;
    this.hide = true;
  }
  
  toggle(){
    this.hide = !this.hide;
  }
}

@Component({
  selector: 'myapp',
  templateUrl: "http://port-8080.angular-brisaning208623.codeanyapp.com/curso/resultado/ts-app4-template"
})
class MyappComponent {
  objecto: Object[];
  
  constructor() {
    this.objecto = [
      new Myapp({titulo: "Vencer el mal", mensaje:"El mal se vence alciendo el bien"}),
      new Myapp({titulo: "Secreto del amor", mensaje:"Sólo se puede amar a otros, cuando se tiene amor propio"}),
      new Myapp({titulo: "El perdón", mensaje:"Mira los niños, ellos olvidan las ofensas porque su mente solo retiene lo importante, que les permite crecer y aprender"})
    ];
  }  
}

@NgModule({
  imports: [BrowserModule],
  declarations: [MyappComponent],
  bootstrap: [MyappComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);