import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Component} from '@angular/core';

@Component({
  selector: 'myapp',
  template: `
    <h2>{{ titulo }}</h2>
    <p>{{ mensaje }}</p>
  `
})
class MyappComponent {
   titulo:string ;
  mensaje:stirng;
  constructor () {
    this.titulo = "Algun titulo";
    this.mensaje = "Mesaje de texto";
  }
}

@NgModule({
  imports: [BrowserModule],
  declarations: [MyappComponent],
  bootstrap: [MyappComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);